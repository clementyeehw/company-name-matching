# Import Python libraries
import os
import pandas.io.sql as psql
import pyodbc


# Initialise global variables
download_path = r'C:\Risk\Dropbox (SciBetaTeam)\SciBetaTeam\Research\Database'
       
         
# Establish connection to Microsoft SQL server and make Universe file
def make_universe():
    '''
    Establish connection to Microsoft SQL server and make Universe file.
    Return:
        universe   : DataFrame containing the universe constituents
    '''
    # Rename current universe file as universe_past #
    for file in os.listdir(download_path):
        if file == 'universe_current.xlsx':
            os.remove(download_path + r'\universe_past.xlsx')
            os.rename(download_path + '\\' + file, download_path + r'\universe_past.xlsx')
            break
            
    # Establish connection to Microsoft SQL server #
    conn = pyodbc.connect(r'DRIVER={ODBC Driver 17 for SQL Server};'
                          r'SERVER=DBxPri.scibeta.net\SciBeta;'
                          r'DATABASE=SGP;'
                          r'UID=sgpscibeta;'
                          r'PWD=sgpscibeta')
    
    # Read sql query as string #
    sql_file = open(download_path + r'\makeUniverse.sql')
    sql_query = sql_file.read()
    
    # Run sql query and download latest universe file #
    universe = psql.read_sql(sql_query, conn)
    universe.drop_duplicates(subset=['sciBetaId'], inplace=True)
    universe.to_excel(download_path + r'\universe_current.xlsx', 
                      sheet_name = 'Universe',
                      index = False)
    
    return universe

if __name__ == '__main__':
    make_universe()