# Import Python Libraries #
import Levenshtein as lev
import numpy as np
import pandas as pd
import re


# Define company name matching function #
def preprocess_names(df, abb_patterns):
    '''
    Preprocess company names based on input DataFrame. 
    Parameter:
        df            : Input DataFrame containing the company names to be preprocessed
        abb_patterns  : String containing the company abbreviations to be eliminated
    Return:
        df            : Output DataFrame containing the company names after preprocessing
    '''
    # Initiate parameter #
    companies_new = []
    
    # Review all columns and identify whether 'Company' column is present #
    try:
        company_column = [column for column in df.columns if column == 'Company'][0]
    except Exception:
        # Print error status message #
        print('The columns do not contain a Company column. Please review dataset and '
              'ensure the Company column is present.')
    else:
        ## Change company name to upper case ##
        df[company_column] = df[company_column].str.upper() 
        ## Create a list for company names ##
        companies =  list(df[company_column])
        ## Iterate over company_list to remove abb_patterns ##
        for company in companies:
            for match in re.finditer(abb_patterns, company):
                pattern = match.group(0)
                if pattern is not None:
                    company = re.sub(r'\b%s\b' % pattern, '', company).strip() 
            company = re.sub(r'\bTHE\b', ' ', company).strip()
            company = re.sub(r'[^\w\s]', ' ', company).strip()
            companies_new.append(re.sub(r'\s{2,}', ' ', company))
        ## Create new column in df ##
        df['COMPANY_NEW'] = companies_new
        
        return df
    

def get_closest_match(company_df_1, company_df_2, minimum_ratio=0.7, top_few=3):
    '''
    Computes the Levenshtein distance between 2 strings 
    Parameters:
        company_df_1    : DataFrame containing the first set of company names (i.e. SBSU_companies)
        company_df_2    : DataFrame containing the second set of company names
        minimum_ratio   : Minimum threshold to be met for similarity matching
        top_few         : Maximum number of records, arranged in descending ratio
    Return :
        table           : Pandas DataFrame containing the Levenshtein ratios
        
    '''
    # Initiate parameters #
    company_list_1 = list(company_df_1['COMPANY_NEW'])
    company_list_2 = list(company_df_2['COMPANY_NEW'])
        
    # Make SBSU (rows) and cannabis (columns) companies into matrix of zeroes #
    rows = len(company_list_2) 
    cols = len(company_list_1) # SBSU_companies
    distance = np.zeros((rows, cols), dtype=float)
    
    # Compute the distance #
    for j in range(cols):
        for i in range(rows):
            ## Round the ratios to 3 decimal places ##
            distance[i][j] = round(lev.ratio(company_list_2[i], company_list_1[j]), 3)
    
    # Import the distances into Pandas DataFrame #
    table = pd.DataFrame(distance, index=company_list_2, columns=company_list_1)
    
    # Get top n ratios per table row #
    topFew = np.argsort(-table.values, axis=1)[:, :top_few]
    arr = []
    for i in range(len(topFew)):
        temp = []
        for j in range(top_few):
            if table.iloc[i, topFew[i][j]] < minimum_ratio :
                temp.append((np.nan, np.nan))
            else:
                df_ = company_df_1.loc[company_df_1['COMPANY_NEW'] == table.columns.values[topFew[i][j]]]
                if len(df_) == 1 or (len(df_) > 1 and j == 0):
                    temp.append((company_df_1.loc[company_df_1['COMPANY_NEW'] == table.columns.values[topFew[i][j]], 'Company'].iloc[0], 
                                 company_df_1.loc[company_df_1['COMPANY_NEW'] == table.columns.values[topFew[i][j]], 'sciBetaId'].iloc[0],
                                 table.iloc[i, topFew[i][j]]))
                elif (len(df_) > 1) and j == 1:
                    temp.append((company_df_1.loc[company_df_1['COMPANY_NEW'] == table.columns.values[topFew[i][j]], 'Company'].iloc[1], 
                                 company_df_1.loc[company_df_1['COMPANY_NEW'] == table.columns.values[topFew[i][j]], 'sciBetaId'].iloc[1],
                                 table.iloc[i, topFew[i][j]]))
                elif (len(df_) > 2) and j == 2:
                    temp.append((company_df_1.loc[company_df_1['COMPANY_NEW'] == table.columns.values[topFew[i][j]], 'Company'].iloc[2], 
                                 company_df_1.loc[company_df_1['COMPANY_NEW'] == table.columns.values[topFew[i][j]], 'sciBetaId'].iloc[2],
                                 table.iloc[i, topFew[i][j]]))
                    
        arr.append(temp)
        
    # Import the ratios into Pandas DataFrame and manipulate the columns #
    top_ratios = pd.DataFrame(arr, index=table.index)
    name_columns = ['High_Sim_Name_' + str(i+1) for i in range(len(top_ratios.columns))]
    top_ratios.columns = name_columns
    for i in range(len(name_columns)):
        top_ratios['High_Sim_Ratio_%s' % str(i+1)] = top_ratios.loc[:, 'High_Sim_Name_%s' % str(i+1)].str[-1]
        top_ratios['High_Sim_ISIN_%s' % str(i+1)] = top_ratios.loc[:, 'High_Sim_Name_%s' % str(i+1)].str[1]
        top_ratios['High_Sim_Name_%s' % str(i+1)] = top_ratios.loc[:, 'High_Sim_Name_%s' % str(i+1)].str[0]
    # Rearrange the columns #
    columns = [column for i in range(len(name_columns)) \
               for column in list(top_ratios.columns) 
               if int(column.split('_')[-1]) == i+1]
    top_ratios = top_ratios[columns]
    
    return table, top_ratios


# Initiate parameters #
## String containing the local download filepath - Update path when necessary ##
download_path = r'C:\Risk\Dropbox\ESG Research\Energy Extractives'
## Create variable that contains common company abbreviations ##
abb_patterns = r'\b(AND|&)(\sCOMPANY|\sCO)\b|\bA\.?/?B\.?\b|\bA\.?/?G\.?\b|\bA\.?\s?/?S\.?\b|\bANONIM SIRKETI\b|' + \
               r'\bAKTIEBOLAG\b|\bAKTIENGESELLSCHAFT\b|\bA\.?S\.?A\.?\b|\bB\.?/?V\.?\b|\bBERHAD\b|\bBHD\.?\b|' + \
               r'\bCOMPAGNIE\b|\b(CO,?\.?|COMPANY)(,?\sINC\.?|,?\sINCORPORATED)\b|\b(CO,?\.?|COMPANY) INC(\./THE)?\b|' + \
               r'\b(CO,?\.?|COMPANY)(\sLTD\.?|\sLIMITED)?\b|\b(CORP\.?|CORPORATION)(\sLTD\.?|\sLIMITED)?\b|' + \
               r'\bGESELLSCHAFT MIT BESCHRÄNKTER HAFTUNG\b|\bGMBH(\s&\sCO\.?\s?KG)?\b|\bGRUPO\b|\bGROUPE\b|' + \
               r'\bHLDGS?\b|\bHOLDINGS?\b|\bINC\.?\b|\bINCORPORATED\b|\bIND\.?\b|' + \
               r'\bJOINT STOCK(\sCOMPANY|\sCO)\b|\bJ(\.\s?)?S(\.\s?)?C(\.\s?)?\b|\bKONINKLIJKE?\b|' + \
               r'\bLIMITED LIABILITY(\sCOMPANY|\sCO)\b|\bLIMITED L(\.\s?)?C(\.\s?)?\b|\bL(\.\s?)?L(\.\s?)?C(\.\s?)?\b|' + \
               r'\bLIMITED\b|\bL(\.\s?)?P(\.\s?)?\b|\b(PTY\s)?LTD\.?\b|\bN\.?,?/?V\.?,?\b|' + \
               r'\bOSAKEYHTIÖ\b|\b[ØO]\.?/?Y\.?\b|\bOYJ\b|\bPERSEROAN TERBATAS\b|\bP\.?T\.?\b|' + \
               r'\bPUBL\b|\bPUBLIC J(\.\s?)?S(\.\s?)?C(\.\s?)?\b|\bP(\.\s?)?J(\.\s?)?S(\.\s?)?C(\.\s?)?\b|' + \
               r'\bPUBLIC COMPANY LIMITED\b|\bPUBLIC JOINT STOCK (CO.?|COMPANY)\b|\bPUBLIC COMPANY LIMITED\b|' + \
               r'\bP(\.\s?)?C(\.\s?)?L(\.\s?)?\b|\bP(\.\s?)?L(\.\s?)?C(\.\s?)?\b|\bPUBLIC LIMITED(\sCO|\sCOMPANY)\b|' + \
               r'\bQ(\.\s?)?P(\.\s?)?S(\.\s?)?C(\.\s?)?\b|\bQ(\.\s?)?S(\.\s?)?C(\.\s?)?\b|' + \
               r'\bS\.?A\.?B\.?(\sD\.?E\.?\sC\.?V\.?)?\b|\bS\.?,?/?A\.?,?\b|' + \
               r'\bS(\.\s?)?A(\.\s?)?E(\.\s?)?\b|\bS\.?,?/?E\.?,?\b|' + \
               r'\bSOCIETÀ PER AZIONI\b|\bSOCIETAS EUROPEAE\b|\bSOCIÉTÉ ANONYME\b|\bS(\.\s?)?P(\.\s?)?A(\.\s?)?\b|' + \
               r'\bT(\.\s?)?B(\.\s?)?K(\.\s?)?\b|\(PUB.?\)|/\w+'

# Import relevant datasets and preprocess the company names #
SBSU_data = pd.read_excel(download_path + r'\Universe.xlsx', sheet_name='Cum_All_Qtrs_Unique')
SBSU_data = preprocess_names(SBSU_data, abb_patterns)
ext_data = pd.read_excel(download_path + r'\path', sheet_name='to_fill_sheet')
ext_data = preprocess_names(ext_data, abb_patterns)

# Run the Levenshtein similarity test and merge the datasets #
table, top_ratios = get_closest_match(SBSU_data, ext_data, minimum_ratio=0.5, top_few=3)
top_ratios.to_excel(download_path + r'\filename.xlsx', sheet_name='RESULTS')