
create table #tmpLst (name varchar(128), sedol varchar(7) null, isin varchar(12) null, searchName varchar(128) null)
truncate table #tmpLst;
-- This part as be let as an example to feed the table
insert #tmpLst (name, sedol, isin) values ('Incitec Pivot Ltd','B3D1D34','AU0000IPLDA3')
insert #tmpLst (name, sedol, isin) values ('Incitec Pivot Ltd','6673042','AU000000IPL1')
insert #tmpLst (name, sedol, isin) values ('Barrick Gold Corp','2024644','CA0679011084')
insert #tmpLst (name, sedol, isin) values ('Barrick Gold Corp','2024677','CA0679011084')
insert #tmpLst (name, sedol, isin) values ('Potash Corp of Saskatchewan Inc','2696980','CA73755L1076')
insert #tmpLst (name, sedol, isin) values ('Potash Corp of Saskatchewan Inc','2696377','CA73755L1076')
insert #tmpLst (name, sedol, isin) values ('Dongfeng Motor Group Co Ltd','B0PH5N3','CNE100000312')
insert #tmpLst (name, sedol, isin) values ('Safran SA','B058TZ6','FR0000073272')
insert #tmpLst (name, sedol, isin) values ('Serco Group PLC','0797379','GB0007973794')
insert #tmpLst (name, sedol, isin) values ('Elbit Systems Ltd','2311614','IL0010811243')
insert #tmpLst (name, sedol, isin) values ('Elbit Systems Ltd','6308913','IL0010811243')

-- feed searchName based column on name column
update #tmpLst set searchName=rtrim(Name);

/* Remove punctuation to simplify the list of insctruction after
update #tmpLst set searchName=REPLACE(searchName,',','') where searchName like '%,%'
update #tmpLst set searchName=REPLACE(searchName,'.','') where searchName like '%.%'
update #tmpLst set searchName=REPLACE(searchName,' &','') where searchName like '% &'
*/

update #tmpLst set searchName=REPLACE(searchName,' AS','') where searchName like '% AS'
update #tmpLst set searchName=REPLACE(searchName,' BV','') where searchName like '% BV'
update #tmpLst set searchName=REPLACE(searchName,' Bhd','') where searchName like '% Bhd'
update #tmpLst set searchName=REPLACE(searchName,' Co Inc','') where searchName like '% Co Inc'
update #tmpLst set searchName=REPLACE(searchName,' Co Inc/The','') where searchName like '% Co Inc/The'
update #tmpLst set searchName=REPLACE(searchName,' Co Ltd','') where searchName like '% Co Ltd'
update #tmpLst set searchName=REPLACE(searchName,' Co','') where searchName like '% Co'
update #tmpLst set searchName=REPLACE(searchName,' Corp','') where searchName like '% Corp'
update #tmpLst set searchName=REPLACE(searchName,' Corp.','') where searchName like '% Corp.'
update #tmpLst set searchName=REPLACE(searchName,' Inc','') where searchName like '% Inc'
update #tmpLst set searchName=REPLACE(searchName,' Inc.','') where searchName like '% Inc.'
update #tmpLst set searchName=REPLACE(searchName,' LLC','') where searchName like '% LLC'
update #tmpLst set searchName=REPLACE(searchName,' Ltd','') where searchName like '% Ltd'
update #tmpLst set searchName=REPLACE(searchName,' Ltd.','') where searchName like '% Ltd.'
update #tmpLst set searchName=REPLACE(searchName,' NV','') where searchName like '% NV'
update #tmpLst set searchName=REPLACE(searchName,' PCL','') where searchName like '% PCL'
update #tmpLst set searchName=REPLACE(searchName,' Plc','') where searchName like '% Plc'
update #tmpLst set searchName=REPLACE(searchName,' Plc.','') where searchName like '% Plc.'
update #tmpLst set searchName=REPLACE(searchName,' SA','') where searchName like '% SA'
update #tmpLst set searchName=REPLACE(searchname,' & CO','') where searchname like '% & CO'
update #tmpLst set searchName=REPLACE(searchname,' (AU)','') where searchname like '% (AU)'
update #tmpLst set searchName=REPLACE(searchname,' (GB)','') where searchname like '% (GB)'
update #tmpLst set searchName=REPLACE(searchname,' A','') where searchname like '% A'
update #tmpLst set searchName=REPLACE(searchname,' A/S','')
update #tmpLst set searchName=REPLACE(searchname,' AB','') where searchname like '% AB'
update #tmpLst set searchName=REPLACE(searchname,' AD','') where searchname like '% AD'
update #tmpLst set searchName=REPLACE(searchname,' AG','') where searchname like '% AG'
update #tmpLst set searchName=REPLACE(searchname,' AG','') where searchname like '%AG'
update #tmpLst set searchName=REPLACE(searchname,' AS','') where searchname like '% AS'
update #tmpLst set searchName=REPLACE(searchname,' ATK','')  where searchname like '% ATK'
update #tmpLst set searchName=REPLACE(searchname,' B','') where searchname like '% B'
update #tmpLst set searchName=REPLACE(searchname,' BHD','') where searchname like '%BHD'
update #tmpLst set searchName=REPLACE(searchname,' CO','') where searchname like '% CO'
update #tmpLst set searchName=REPLACE(searchname,' Co Ltd','')  where searchname like '% Co'
update #tmpLst set searchName=REPLACE(searchname,' Co','') where searchname like '% Co'
update #tmpLst set searchName=REPLACE(searchname,' Co., Ltd.','') where searchname like '%  Co., Ltd.'
update #tmpLst set searchName=REPLACE(searchname,' corporation','') where searchname like '% corporation'
update #tmpLst set searchName=REPLACE(searchname,' Corp.','') where searchname like '% Corp.'
update #tmpLst set searchName=REPLACE(searchname,' DD','') where searchname like '% DD'
update #tmpLst set searchName=REPLACE(searchname,' GMBH','') where searchname like '%GMBH'
update #tmpLst set searchName=REPLACE(searchname,' H','') where searchname like '% H'
update #tmpLst set searchName=REPLACE(searchname,' IND','') where searchname like '% IND'
update #tmpLst set searchName=REPLACE(searchname,' Inc.','')  where searchname like '% Inc.'
update #tmpLst set searchName=REPLACE(searchname,' JSC','')  where searchname like '% JSC'
update #tmpLst set searchName=REPLACE(searchname,' N,V,','') where searchname like '% N,V,'
update #tmpLst set searchName=REPLACE(searchname,' N,V,','') where searchname like '%N,V,'
update #tmpLst set searchName=REPLACE(searchname,' N.V.','') where searchname like '% N.V.'
update #tmpLst set searchName=REPLACE(searchname,' N.V.','') where searchname like '%N.V.'
update #tmpLst set searchName=REPLACE(searchname,' NV','') where searchname like '% NV'
update #tmpLst set searchName=REPLACE(searchname,' NV','') where searchname like '%NV'
update #tmpLst set searchName=REPLACE(searchname,' ON','') where searchname like '% ON'
update #tmpLst set searchName=REPLACE(searchname,' PJSC','')  where searchname like '% PJSC'
update #tmpLst set searchName=REPLACE(searchname,' PLC','')  where searchname like '% PLC'
update #tmpLst set searchName=REPLACE(searchname,' PN','') where searchname like '% PN'
update #tmpLst set searchName=REPLACE(searchname,' S,A,','') where searchname like '% S,A,'
update #tmpLst set searchName=REPLACE(searchname,' S,A,','') where searchname like '%S,A,'
update #tmpLst set searchName=REPLACE(searchname,' S.A.','') where searchname like '% S.A.'
update #tmpLst set searchName=REPLACE(searchname,' S.E.','') where searchname like '% S.E.'
update #tmpLst set searchName=REPLACE(searchname,' S.p.A.','') where searchname like '% S.p.A.'
update #tmpLst set searchName=REPLACE(searchname,' SA','') where searchname like '% SA'
update #tmpLst set searchName=REPLACE(searchname,' SA','') where searchname like '%SA'
update #tmpLst set searchName=REPLACE(searchname,' SE','') where searchname like '% SE'
update #tmpLst set searchName=REPLACE(searchname,' SpA','') where searchname like '% SpA'
update #tmpLst set searchName=REPLACE(searchname,' Tbk PT','') where searchname like '% Tbk PT'
update #tmpLst set searchName=REPLACE(searchname,' V','') where searchname like '% V'
update #tmpLst set searchName=REPLACE(searchname,' VZG','') where searchname like '% VZG'
update #tmpLst set searchName=REPLACE(searchname,' corp','') where searchname like '% corp'
update #tmpLst set searchName=REPLACE(searchname,' corporation','')
update #tmpLst set searchName=REPLACE(searchname,' inc','')  where searchname like '%inc'
update #tmpLst set searchName=REPLACE(searchname,' limited','') where searchname like '%limited'
update #tmpLst set searchName=REPLACE(searchname,' ltd','') where searchname like '% ltd'
update #tmpLst set searchName=REPLACE(searchname,' ltd,','') where searchname like '% ltd,'
update #tmpLst set searchName=REPLACE(searchname,' ltd.','') where searchname like '% ltd.'
update #tmpLst set searchName=REPLACE(searchname,' plc','') where searchname like '%plc'
update #tmpLst set searchName=REPLACE(searchname,'(RUB)','') where searchname like '%(RUB)'
update #tmpLst set searchName=REPLACE(searchname,', Inc.','')  where searchname like '%, Inc.'
update #tmpLst set searchName=REPLACE(searchname,'/The','')  where searchname like '%/The'
update #tmpLst set searchName=REPLACE(searchname,'limited','')
update #tmpLst set searchName=replace(searchName,'Â ',' ');
update #tmpLst set searchName=replace(searchName,'Â ','');

update #tmpLst set searchName='Alliance One' where name='Alliance One International Inc.'
update #tmpLst set searchName='Coka Duvanska Industrija' where name='Coka Duvanska Industrija AD Coka'
update #tmpLst set searchName='Duvanska Industrija' where name='Duvanska Industrija AD Bujanovac '
update #tmpLst set searchName='Universal' where name='Universal Corp/VA'
update #tmpLst set searchName='VOLKSWAGEN' where searchname like 'VOLKSWAGEN%'

update #tmpLst set searchName=ltrim(searchName);
update #tmpLst set searchName=rtrim(searchname)



-- then you can work on comparison between this #tmpLst and another one
select		*
from		#tmpLst		t
left join	refTable	r on CHARINDEX(t.searchName, r.companyName collate French_CI_AS, 1)  > 0