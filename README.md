# Overview

Computes Levenshtein similarity ratio between 2 given company names (i.e. strings) and returns top 3 similar results.  

---

## Execution

1. Before executing the script, please update the filepaths and sheets used for master (i.e. Scientific Beta Universe), slave (i.e. sourced externally) and output files.
2. The column name assigned for the company column in the external file is 'Company'. 
3. Execute the script and manually verify the results in the output file.

---
