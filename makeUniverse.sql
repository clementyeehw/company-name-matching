SELECT sciBetaId, companyName, sources
FROM (
		SELECT temp2.*, ROW_NUMBER() OVER (PARTITION BY sciBetaId ORDER BY sources DESC) AS RowNumber
		FROM (  SELECT *
				FROM (	SELECT sciBetaId, name AS companyName, sources
						FROM (	SELECT assetId AS sciBetaId, MAX(date) as maxDate, 'universe' AS sources
								FROM dbo.pubMarket
								GROUP BY assetId) AS maxDateSec
						INNER JOIN dbo.pubMarket
						ON maxDateSec.sciBetaId = dbo.pubMarket.assetId AND 
						   maxDateSec.maxDate = dbo.pubMarket.date
					 ) AS temp
				UNION
				SELECT universe AS sciBetaId, ciqCompanyName AS companyName, 'candidates' as sources
				FROM dbo.sciCandidates
				WHERE universe IS NOT NULL
			) AS temp2
	) AS combined
WHERE combined.RowNumber = 1
ORDER BY combined.sciBetaId
;